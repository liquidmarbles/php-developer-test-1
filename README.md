Given this basic Symfony2 project, look to improve the implementation. Some suggestions as to what could be added or improved:

	- Code quality
	- Code comments
	- Polymorphism / inheritance
	- Error handling
	- Feedback / output
	- Additional pages for further detail
	- Data storage and relationships
	- Testing
	
To get the project running, you must first install the vendors via composer. Visit this page for more information: http://symfony.com/doc/current/book/installation.html