<?php

namespace DestinationCms\SchoolBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class SchoolController extends Controller
{	
	/**
	 * @param Request $request
	 *
	 * @return Response
	 */
	public function indexAction(Request $request)
	{
		$schoolManager = $this->container->get('dcms.school_manager');
		$schools = $schoolManager->getSchools();
		
		return $this->render(
			'DestinationCmsSchoolBundle:School:index.html.twig',
			array(
				'schools' => $schools,
			)
		);
	}
	
	/**
	 * @param Request $request
	 * @param string $reference A school's reference
	 *
	 * @return Response
	 */
	public function schoolAction(Request $request, $reference)
	{
		// fetch school
		$schoolManager = $this->container->get('dcms.school_manager');
		$school = $schoolManager->getSchool($reference);
		
		// fetch courses
		$courseManager = $this->container->get('dcms.course_manager');
		$courses = array();
		foreach($school->courses as $courseReference)
		{
			$courses[] = $courseManager->getCourse($courseReference);
		}
		
		// fetch teachers
		$teacherManager = $this->container->get('dcms.teacher_manager');
		$teachers = array();
		foreach($school->teachers as $teacherId)
		{
			$teachers[] = $teacherManager->getTeacher($teacherId);
		}
		
		// fetch students
		$studentManager = $this->container->get('dcms.student_manager');
		$students = array();
		foreach($school->students as $studentId)
		{
			$students[] = $studentManager->getStudent($studentId);
		}
	
		// render view
		return $this->render(
			'DestinationCmsSchoolBundle:School:school.html.twig',
			array(
				'school' => $school,
				'courses' => $courses,
				'teachers' => $teachers,
				'students' => $students,
			)
		);
	}
}