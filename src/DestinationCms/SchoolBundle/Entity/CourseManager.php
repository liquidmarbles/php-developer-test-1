<?php

namespace DestinationCms\SchoolBundle\Entity;

class CourseManager
{
	private $courses;
	
	public function __construct()
	{
		$englishCourse = new Course();
		$englishCourse->reference = 'ENG01';
		$englishCourse->name = 'English';
		$englishCourse->topics = array('Poetry', 'Literacy');
		
		$physicsCourse = new Course();
		$physicsCourse->reference = 'PHYS01';
		$physicsCourse->name = 'Physics';
		$physicsCourse->topics = array('Forces and Motion', 'Energy');
		
		$biologyCourse = new Course();
		$biologyCourse->reference = 'BIO01';
		$biologyCourse->name = 'Biology';
		$biologyCourse->topics = array('Plants', 'Genetics');
		
		$informationTechnologyCourse = new Course();
		$informationTechnologyCourse->reference = 'ITC01';
		$informationTechnologyCourse->name = 'Information Technology';
		$informationTechnologyCourse->topics = array('Microsoft Office', 'Internet');
		
		$mathCourse = new Course();
		$mathCourse->reference = 'MTH01';
		$mathCourse->name = 'Maths';
		$mathCourse->topics = array('Algebra', 'Trigonometry');
		
		$this->courses = array(
			$englishCourse->reference => $englishCourse,
			$physicsCourse->reference => $physicsCourse,
			$biologyCourse->reference => $biologyCourse,
			$informationTechnologyCourse->reference => $informationTechnologyCourse,
			$mathCourse->reference => $mathCourse,
		);
	}
	
	/**
	 * @return array
	 */
	public function getCourses()
	{
		return $this->courses;
	}
	
	public function getCourse($reference)
	{
		return $this->courses[$reference];
	}
}