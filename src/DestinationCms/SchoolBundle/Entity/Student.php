<?php

namespace DestinationCms\SchoolBundle\Entity;

class Student
{
	private static $nextId = 1;
	
	public $id;	
	public $name;
	public $gender;
	public $dob;	
	public $address;
	public $town;
	public $county;
	public $postcode;
	public $courses;
		
	public function __construct()
	{
		$this->id = self::$nextId++;
	}
	
	public function addCourse($course)
	{
		$this->courses[] = $course;
	}
}