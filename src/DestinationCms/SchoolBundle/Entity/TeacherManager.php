<?php

namespace DestinationCms\SchoolBundle\Entity;

class TeacherManager
{
	private $teachers;
	
	public function __construct()
	{
		$peterSmith = new Teacher();
		$peterSmith->name = 'Peter Smith';
		$peterSmith->gender = 'Male';
		$peterSmith->dob = new \DateTime('20 September 1980');
		$peterSmith->salary = 17000;
		$peterSmith->email = 'peter.smith@exeter-school.com';
		$peterSmith->address = '3 Big Road';
		$peterSmith->town = 'Biggleton';
		$peterSmith->county = 'Big';
		$peterSmith->postcode = 'BG4 4FN';
		$peterSmith->courses = array('PHYS01', 'BIO01');
		
		$joeBloggs = new Teacher();
		$joeBloggs->name = 'Joe Bloggs';
		$joeBloggs->gender = 'Male';
		$joeBloggs->dob = new \DateTime('11 March 1976');
		$joeBloggs->salary = 21000;
		$joeBloggs->email = 'joe.bloggs@exeter-school.com';
		$joeBloggs->address = '2 Big Road';
		$joeBloggs->town = 'Biggleton';
		$joeBloggs->county = 'Big';
		$joeBloggs->postcode = 'BG4 4FN';
		$joeBloggs->courses = array('MTH01', 'ITC01');
		
		$sarahParker = new Teacher();
		$sarahParker->name = 'Sarah Parker';
		$sarahParker->gender = 'Female';
		$sarahParker->dob = new \DateTime('3 June 1984');
		$sarahParker->salary = 17000;
		$sarahParker->email = 'sarah-parker@honiton-school.com';
		$sarahParker->address = '4 Big Road';
		$sarahParker->town = 'Biggleton';
		$sarahParker->county = 'Big';
		$sarahParker->postcode = 'BG4 4FN';
		$sarahParker->courses = array('ENG01');
		
		$this->teachers = array(
			$peterSmith->id => $peterSmith,
			$joeBloggs->id => $joeBloggs,
			$sarahParker->id => $sarahParker,
		);
	}
	
	/**
	 * @return array
	 */
	public function getTeachers()
	{
		return $this->teachers;
	}
	
	public function getTeacher($id)
	{
		return $this->teachers[$id];
	}
}