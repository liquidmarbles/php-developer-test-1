<?php

namespace DestinationCms\SchoolBundle\Entity;

class StudentManager
{
	private $students;
	
	public function __construct()
	{
		$suzanWebb = new Student();
		$suzanWebb->name = 'Suzan Webb';
		$suzanWebb->gender = 'Female';
		$suzanWebb->dob = new \DateTime('9 April 2005');
		$suzanWebb->address = '5 Big Road';
		$suzanWebb->town = 'Biggleton';
		$suzanWebb->county = 'Big';
		$suzanWebb->postcode = 'BG4 4FN';
		$suzanWebb->courses = array('PHYS01', 'BIO01', 'ENG01', 'MTH01', 'ITC01');
		
		$peterWren = new Student();
		$peterWren->name = 'Peter Wren';
		$peterWren->gender = 'Male';
		$peterWren->dob = new \DateTime('19 April 2005');
		$peterWren->address = '6 Big Road';
		$peterWren->town = 'Biggleton';
		$peterWren->county = 'Big';
		$peterWren->postcode = 'BG4 4FN';
		$peterWren->courses = array('PHYS01', 'BIO01', 'ENG01', 'MTH01', 'ITC01');
		
		$chrisWalker = new Student();
		$chrisWalker->name = 'Chris Walker';
		$chrisWalker->gender = 'Male';
		$chrisWalker->dob = new \DateTime('12 February 2006');
		$chrisWalker->address = '7 Big Road';
		$chrisWalker->town = 'Biggleton';
		$chrisWalker->county = 'Big';
		$chrisWalker->postcode = 'BG4 4FN';
		$chrisWalker->courses = array('PHYS01', 'BIO01', 'ENG01', 'MTH01', 'ITC01');
		
		$peterSmith = new Student();
		$peterSmith->name = 'Peter Smith';
		$peterSmith->gender = 'Male';
		$peterSmith->dob = new \DateTime('20 September 2005');
		$peterSmith->address = '8 Big Road';
		$peterSmith->town = 'Biggleton';
		$peterSmith->county = 'Big';
		$peterSmith->postcode = 'BG4 4FN';
		$peterSmith->courses = array('PHYS01', 'BIO01');
		
		$joeBloggs = new Student();
		$joeBloggs->name = 'Joe Bloggs';
		$joeBloggs->gender = 'Male';
		$joeBloggs->dob = new \DateTime('11 March 2005');
		$joeBloggs->address = '9 Big Road';
		$joeBloggs->town = 'Biggleton';
		$joeBloggs->county = 'Big';
		$joeBloggs->postcode = 'BG4 4FN';
		$joeBloggs->courses = array('MTH01', 'ITC01');
		
		$sarahParker = new Student();
		$sarahParker->name = 'Sarah Parker';
		$sarahParker->gender = 'Female';
		$sarahParker->dob = new \DateTime('3 June 2005');
		$sarahParker->address = '10 Big Road';
		$sarahParker->town = 'Biggleton';
		$sarahParker->county = 'Big';
		$sarahParker->postcode = 'BG4 4FN';
		$sarahParker->courses = array('ENG01');
		
		$this->students = array(
			$suzanWebb->id => $suzanWebb,
			$peterWren->id => $peterWren,
			$chrisWalker->id => $chrisWalker,
			$peterSmith->id => $peterSmith,
			$joeBloggs->id => $joeBloggs,
			$sarahParker->id => $sarahParker,
		);
	}
	
	/**
	 * @return array
	 */
	public function getStudents()
	{
		return $this->students;
	}
	
	public function getStudent($id)
	{
		return $this->students[$id];
	}
}