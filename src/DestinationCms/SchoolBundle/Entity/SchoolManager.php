<?php

namespace DestinationCms\SchoolBundle\Entity;

class SchoolManager {
	private $schools;
	
	public function __construct() {
		$exeterSchool = new School();
		$exeterSchool->reference = 'EXETER';
		$exeterSchool->name = 'Exeter School';
		$exeterSchool->addCourses(array('ENG01', 'PHYS01', 'BIO01', 'ITC01'));
		$exeterSchool->addTeacher(1);
		$exeterSchool->addTeacher(2);
		$exeterSchool->addStudent(1);
		$exeterSchool->addStudent(2);
		$exeterSchool->addStudent(3);
		
		$honitonSchool = new School();
		$honitonSchool->reference = 'HONITON';
		$honitonSchool->name = 'Honiton School';
		$honitonSchool->addCourses(array('ENG01', 'PHYS01', 'BIO01', 'ITC01'));
		$honitonSchool->addTeacher(3);
		$honitonSchool->addStudent(4);
		$honitonSchool->addStudent(5);
		
		$collumptonSchool = new School();
		$collumptonSchool->reference = 'COLLUMPTON';
		$collumptonSchool->name = 'Collumpton School';
		$collumptonSchool->addCourses(array('ENG01', 'PHYS01', 'BIO01', 'MTH01', 'MTH02'));
		$collumptonSchool->addStudent(6);
		
		$this->schools = array(
			$exeterSchool->reference => $exeterSchool,
			$honitonSchool->reference => $honitonSchool,
			$collumptonSchool->reference => $collumptonSchool,
		);
	}
	
	/**
	 * @return array
	 */
	public function getSchools()
	{
		return $this->schools;
	}
	
	/**
	 * @param string $reference A school's reference
	 * 
	 * @return School
	 */
	public function getSchool($reference)
	{
		return $this->schools[$reference];
	}
}