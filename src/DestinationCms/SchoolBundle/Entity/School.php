<?php

namespace DestinationCms\SchoolBundle\Entity;

class School
{
	public $reference;
	public $name;
	public $courses = array();
	public $teachers = array();
	public $students = array();
		
	public function addCourses(array $courses)
	{
		$this->courses = array_merge($this->courses, $courses);
	}
	
	public function addTeacher($teacher)
	{
		$this->teachers[] = $teacher;
	}
	
	public function addStudent($student)
	{
		$this->students[] = $student;
	}
}