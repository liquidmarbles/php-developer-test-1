<?php

namespace DestinationCms\SchoolBundle\Entity;

class Teacher
{
	private static $nextId = 1;
	
	public $id;
	public $name;
	public $gender;
	public $dob;
	public $salary;
	public $telephone;
	public $email;
	public $address;
	public $town;
	public $county;
	public $postcode;
	public $courses;
	
	public function __construct()
	{
		$this->id = self::$nextId++;
	}
	
	public function addCourse($course)
	{
		$this->courses[] = $course;
	}
}