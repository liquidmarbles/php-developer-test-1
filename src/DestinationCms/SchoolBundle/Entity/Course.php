<?php

namespace DestinationCms\SchoolBundle\Entity;

class Course
{
	public $reference;
	public $name;
	public $topics;
}